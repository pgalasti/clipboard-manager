﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace ClipboardManager2
{
    class ClipboardListener
    {
        private Thread ListeningThread;
        private bool IsRunning;
        private String text;
        

        public String ClipboardText 
        {
            get { return text; } 
        }

        public delegate void ChangedClipboardEventHandler(object sender, EventArgs e);
        public event ChangedClipboardEventHandler Changed;

        protected virtual void OnChanged(EventArgs e)
        {
            if (Changed != null)
                Changed(this, e);
        }

        public void StartListening()
        {
            if(Clipboard.ContainsText())
                text = Clipboard.GetText();

            IsRunning = true;
            ListeningThread = new Thread(ListenForChange);
            ListeningThread.SetApartmentState(ApartmentState.STA);
            ListeningThread.Start();
        }

        public void StopListening()
        {
            IsRunning = false;
        }

        private void ListenForChange()
        {
            while (IsRunning)
            {
                if (Clipboard.ContainsText())
                {
                    if(text != Clipboard.GetText())
                    {
                        text = Clipboard.GetText();
                        OnChanged(EventArgs.Empty);
                    }

                }
                Thread.Sleep(100);
            }
        }
    }
}
