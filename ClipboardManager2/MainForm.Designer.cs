﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ClipboardManager2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainPanel = new Panel();
            this.btnSaveClipboard = new Button();
            this.btnClear = new Button();
            this.cbxAutoSave = new CheckBox();
            this.cbxAlwaysTop = new CheckBox();
            this.btnSaveDirectory = new Button();
            base.SuspendLayout();
            this.mainPanel.AutoScroll = true;
            this.mainPanel.Location = new Point(12, 41);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new Size(757, 10);
            this.mainPanel.TabIndex = 0;
            this.btnSaveClipboard.Location = new Point(552, 12);
            this.btnSaveClipboard.Name = "btnSaveClipboard";
            this.btnSaveClipboard.Size = new Size(75, 23);
            this.btnSaveClipboard.TabIndex = 1;
            this.btnSaveClipboard.Text = "&Save";
            this.btnSaveClipboard.UseVisualStyleBackColor = true;
            this.btnSaveClipboard.Click += new EventHandler(this.btnSaveClipboard_Click);
            this.btnClear.Location = new Point(12, 12);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new Size(75, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new EventHandler(this.btnClear_Click);
            this.cbxAutoSave.AutoSize = true;
            this.cbxAutoSave.Location = new Point(466, 12);
            this.cbxAutoSave.Name = "cbxAutoSave";
            this.cbxAutoSave.Size = new Size(76, 17);
            this.cbxAutoSave.TabIndex = 3;
            this.cbxAutoSave.Text = "&Auto Save";
            this.cbxAutoSave.UseVisualStyleBackColor = true;
            this.cbxAutoSave.CheckedChanged += new EventHandler(this.cbxAutoSave_CheckedChanged);
            this.cbxAlwaysTop.AutoSize = true;
            this.cbxAlwaysTop.Location = new Point(380, 12);
            this.cbxAlwaysTop.Name = "cbxAlwaysTop";
            this.cbxAlwaysTop.Size = new Size(81, 17);
            this.cbxAlwaysTop.TabIndex = 4;
            this.cbxAlwaysTop.Text = "A&lways Top";
            this.cbxAlwaysTop.UseVisualStyleBackColor = true;
            this.cbxAlwaysTop.CheckedChanged += new EventHandler(this.cbxAlwaysTop_CheckedChanged);
            this.btnSaveDirectory.Location = new Point(633, 12);
            this.btnSaveDirectory.Name = "btnSaveDirectory";
            this.btnSaveDirectory.Size = new Size(136, 23);
            this.btnSaveDirectory.TabIndex = 5;
            this.btnSaveDirectory.Text = "Save &Directory";
            this.btnSaveDirectory.UseVisualStyleBackColor = true;
            this.btnSaveDirectory.Click += new EventHandler(this.btnSaveDirectory_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            //base.AutoScaleMode = AutoScaleMode.Font;
            this.AutoScroll = true;
            base.ClientSize = new Size(788, 62);
            base.Controls.Add(this.btnSaveDirectory);
            base.Controls.Add(this.cbxAlwaysTop);
            base.Controls.Add(this.cbxAutoSave);
            base.Controls.Add(this.btnClear);
            base.Controls.Add(this.btnSaveClipboard);
            base.Controls.Add(this.mainPanel);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "MainForm";
            base.ShowIcon = false;
            this.Text = "Clipboard Manager";
            base.Load += new EventHandler(this.MainForm_Load);
            base.ResumeLayout(false);
            base.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button btnSaveClipboard;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox cbxAutoSave;
        private System.Windows.Forms.CheckBox cbxAlwaysTop;
        private System.Windows.Forms.Button btnSaveDirectory;
        private System.Windows.Forms.Panel contentPanel;

    }
}

