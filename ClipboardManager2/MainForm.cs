﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ClipboardManager2
{
    public partial class MainForm : Form
    {
        private const ushort RESIZE_FACTOR = 100;
        private const ushort DEFAULT_HEIGHT = 90;
        private const ushort PADDING = 10;
        private const byte MAX_TEXTBOX_ON_SCREEN = 8;
        private int NextTextBoxPositionY = 0;
        private List<TextBox> TextBoxList = new List<TextBox>();

        private ClipboardHistory History;
        private ClipboardListener Listener;

        public MainForm()
        {
            Listener = new ClipboardListener();
            History = new ClipboardHistory();

            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Height = DEFAULT_HEIGHT;

            if (Clipboard.ContainsText())
                History.CurrentStrings.Add(Clipboard.GetText());

            Listener.Changed += new ClipboardManager2.ClipboardListener.ChangedClipboardEventHandler(InvokeNewClipboardSpace);
            Listener.StartListening();
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            Listener.Changed -= new ClipboardManager2.ClipboardListener.ChangedClipboardEventHandler(InvokeNewClipboardSpace);
            Listener.StopListening();
        }

        private String prepareDirectory()
        {
            StringBuilder filePathBuilder = new StringBuilder(String.Format(@"{0}-{1}-{2}\",
                DateTime.Now.Month,
                DateTime.Now.Day,
                DateTime.Now.Year));

            if (!System.IO.Directory.Exists(filePathBuilder.ToString()))
                System.IO.Directory.CreateDirectory(filePathBuilder.ToString());

            filePathBuilder.Append("data.txt");

            return filePathBuilder.ToString();
        }

        private void btnSaveClipboard_Click(object sender, EventArgs e)
        {
            String fullFilePath = prepareDirectory();

            foreach (String clipboardText in History.CurrentStrings)
                WriteToFile(fullFilePath, clipboardText);
        }

        private void WriteToFile(String filePath, String text)
        {
            System.IO.TextWriter textWriter = new System.IO.StreamWriter(filePath, true);

            textWriter.WriteLine(String.Format("Saved {0}:{1}:{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
            textWriter.WriteLine(text);
            textWriter.WriteLine();

            textWriter.Close();
        }

        private delegate void CreateNewClipboardSpaceDelegate(String data);
        private void InvokeNewClipboardSpace(object sender, EventArgs e)
        {
            foreach (String storedString in History.CurrentStrings)
            {
                if (Listener.ClipboardText.Equals(storedString))
                    return;
            }
            History.CurrentStrings.Add(Listener.ClipboardText);
            mainPanel.Invoke(new CreateNewClipboardSpaceDelegate(CreateNewClipboardSpace), Listener.ClipboardText);
        }

        private void CreateNewClipboardSpace(String data)
        {
            TextBox newTextBox = new TextBox();
            newTextBox.Multiline = true;
            newTextBox.SetBounds(0, NextTextBoxPositionY, mainPanel.Width, RESIZE_FACTOR);
            newTextBox.Text = data;

            NextTextBoxPositionY += (RESIZE_FACTOR + PADDING);
            TextBoxList.Add(newTextBox);
            mainPanel.Controls.Add(newTextBox);

            mainPanel.Height = NextTextBoxPositionY;
            mainPanel.VerticalScroll.Visible = true;
            if (History.CurrentStrings.Count <= MAX_TEXTBOX_ON_SCREEN)
                this.Height += (RESIZE_FACTOR + PADDING);

            base.VerticalScroll.Value = NextTextBoxPositionY;
            base.VerticalScroll.Visible = true;
        }



        private void btnClear_Click(object sender, EventArgs e)
        {
            foreach (TextBox box in TextBoxList)
                mainPanel.Controls.Remove(box);

            Height = DEFAULT_HEIGHT;
            mainPanel.Height = DEFAULT_HEIGHT;
            NextTextBoxPositionY = 0;
            History.CurrentStrings.Clear();
            if (Clipboard.ContainsText())
                History.CurrentStrings.Add(Clipboard.GetText());
            this.VerticalScroll.Value = 0;
            this.VerticalScroll.Visible = false;
            mainPanel.Refresh();
        }

        private void cbxAutoSave_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void cbxAlwaysTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = cbxAlwaysTop.Checked;
        }

        private void btnSaveDirectory_Click(object sender, EventArgs e)
        {
            String filePathBuilder = String.Format("{0}-{1}-{2}",
                DateTime.Now.Month,
                DateTime.Now.Day,
                DateTime.Now.Year);

            String fullPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            fullPath = System.IO.Path.GetDirectoryName(fullPath);
            fullPath += "\\" + filePathBuilder.ToString();

            if (!System.IO.Directory.Exists(fullPath))
            {
                MessageBox.Show("Nothing has been saved today!");
                return;
            }

            String windowsDirectory = Environment.GetEnvironmentVariable("WINDIR");

            System.Diagnostics.Process explorerProc = new System.Diagnostics.Process();
            explorerProc.StartInfo.FileName = windowsDirectory + @"\explorer.exe";
            explorerProc.StartInfo.Arguments = fullPath;
            explorerProc.Start();
        }
    }
}
